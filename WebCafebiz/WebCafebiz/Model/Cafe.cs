﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCafebiz.Model
{
    public class Cafe
    {
        public int CafeId { get; set; }
        public int CafeName { get; set; }
        public DateTime MyProperty { get; set; }
        public string Address { get; set; }
        public int Index { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }

        public Cafe(int cafeId, int cafeName, DateTime myProperty, string address, int index)
        {
            CafeId = cafeId;
            CafeName = cafeName;
            MyProperty = myProperty;
            Address = address;
            Index = index;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
